import React from "react";
import Image from "next/image";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";

import styles from "./Button.module.css";
import { Classnames } from "../../utils";
import Typography from "../Typography/Typography";

const marginRight = {
  marginRight: "1em",
  marginLeft: "0em",
};

const marginLeft = {
  marginRight: "0em",
  marginLeft: "1em",
};
export default function Button({
  text,
  style,
  onClick,
  icon,
  iconFirst,
  className,
}) {
  return (
    <button
      className={Classnames(styles.button, className)}
      style={style}
      onClick={onClick}
    >
      <span>{text}</span>{" "}
      {icon && <span style={iconFirst ? marginRight : marginLeft}>{icon}</span>}
    </button>
  );
}

export function ButtonIcon({ text, style, onClick, icon, iconFirst }) {
  return (
    <button className={styles.button} style={style} onClick={onClick}>
      {text}
    </button>
  );
}

export function GoBack() {
  return (
    <div
      style={{
        backgroundColor: "#E5E5E5",
        width: "100%",
        display: "flex",
        marginBottom: "1em",
        alignItems: "center",
      }}
      className={styles.backButton}
    >
      <FontAwesomeIcon
        icon={faAngleLeft}
        style={{ color: "#0C8DBA", marginRight: ".5em" }}
      />
      <Typography children="Go Back" variant="h6" color="blue" />
    </div>
  );
}

export function Pagination() {
  return (
    <div style={{ display: "flex", margin: "2em 0 4.5em" }}>
      <Button
        text="Previous"
        style={{
          backgroundColor: "rgba(12, 141, 186, 0.5)",
          padding: ".5em 1em",
          width: "auto",
        }}
      />
      {[1, 2, 3, 4, 5].map((item, i) => (
        <Button
          text={item}
          style={{
            backgroundColor: "rgba(12, 141, 186, 0.5)",
            padding: ".5em 1em",
            borderRadius: "8px",
            margin: "0 .5em",
          }}
        />
      ))}
      <Button
        text="Next"
        style={{
          backgroundColor: "rgba(12, 141, 186, 0.5)",
          padding: ".5em 1em",
          width: "auto",
        }}
      />
    </div>
  );
}
