import React from "react";
import ReactStars from "react-rating-stars-component";
import styles from "./Sidebar.module.css";

import Image from "next/image";
import Typography from "../Typography/Typography";
import Button from "../Button/Button";

const colourData = [
  "#74FBFD",
  " #807F26",
  "green",
  "yellow",
  "blue",
  "#F5C3CB",
  "grey",
  "black",
  "orange",
  "violet",
  "#751B7C",
];
const BrandData = [
  {
    name: "Earth Conscious",
    type: "checkbox",
  },
  {
    name: "Fashion Kids",
    type: "checkbox",
  },
  {
    name: "Chirimoya",
    type: "checkbox",
  },
  {
    name: "1st Care",
    type: "checkbox",
  },
  {
    name: "Baby Go",
    type: "checkbox",
  },
  {
    name: "Aile Rabbit",
    type: "checkbox",
  },
];

const DiscountData = [
  {
    name: "10% and above",
    type: "checkbox",
  },
  {
    name: "20% and above",
    type: "checkbox",
  },
  {
    name: "30% and above",
    type: "checkbox",
  },
  {
    name: "40% and above",
    type: "checkbox",
  },
  {
    name: "50% and above",
    type: "checkbox",
  },
  {
    name: "60% and above",
    type: "checkbox",
  },
];

const PriceData = [
  {
    name: "Under ₦ 2000",
    type: "radio",
  },
  {
    name: "₦ 2000 - ₦ 5000",
    type: "radio",
  },
  {
    name: "₦ 5000 - ₦ 10000",
    type: "radio",
  },
  {
    name: "₦ 10000 - ₦ 20000",
    type: "radio",
  },
  {
    name: "₦ 20000 - ₦ 40000",
    type: "radio",
  },
  {
    name: "Above ₦ 40000",
    type: "radio",
  },
];

function ButtonText({ type, text, checked }) {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        marginTop: "1em",
      }}
    >
      <input type={type} checked={checked} name="test" />

      <Typography
        variant="body4"
        children={text}
        color="black"
        style={{ marginLeft: ".9em" }}
      />
    </div>
  );
}

function ButtonRating({ type, value, checked }) {
  const ratingChanged = (newRating) => {
    console.log(newRating);
  };
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        marginTop: "1em",
      }}
    >
      <input type={type} checked={checked} />
      <div style={{ display: "flex", marginLeft: ".5em" }}>
        <ReactStars
          count={5}
          value={value}
          onChange={ratingChanged}
          size={12}
          activeColor="#ffd700"
        />
        <Typography variant="body4" color="grey" children={`(& up)`} />
      </div>
    </div>
  );
}

function MinMaxGo() {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-start",
        marginTop: "0.6em",
      }}
    >
      <input type="select" style={{ width: "61px", height: "30px" }} />

      <Typography
        variant="body4"
        color="black"
        children="To"
        style={{ margin: "0 .5em" }}
      />
      <input type="select" style={{ width: "61px", height: "30px" }} />
      <Button text="Go" style={{ height: "30px", marginLeft: ".5em" }} />
    </div>
  );
}
export default function SideBar() {
  return (
    <div className={styles.container}>
      <Typography
        variant="h3"
        children="Categories"
        color="black"
        style={{ marginBottom: "1em" }}
      />

      <Typography
        variant="body4"
        children="Books"
        color="black"
        style={{ marginBottom: ".5em" }}
      />
      <div className={styles.row}>
        <div style={{ marginRight: ".8em" }}>
          <Image src="/assets/leftArrow.png" alt="me" width="5" height="9" />
        </div>
        <Typography variant="body4" children="Clothing" color="black" />
      </div>
      <div style={{ padding: "1em" }}>
        <ButtonText text="Boy clothings" type="checkbox" />
        <ButtonText text="Girl clothings" type="checkbox" />
      </div>
      <Typography variant="body4" children="Toys" color="black" />
      <Typography
        variant="body4"
        children="Foot Wears"
        color="black"
        style={{ marginTop: ".8em" }}
      />

      <Typography
        variant="h3"
        children="Avg.Customer Review"
        color="black"
        style={{ marginTop: "1em" }}
      />
      <ButtonRating type="checkbox" value={4} />
      <ButtonRating type="checkbox" value={3} />
      <ButtonRating type="checkbox" value={2} />
      <ButtonRating type="checkbox" value={1} />

      <Typography
        variant="h3"
        children="Brand"
        color="black"
        style={{ marginBottom: "1em", marginTop: "1em" }}
      />
      <input
        style={{
          height: "27px",
          padding: ".5em",
          fontSize: ".8rem",
        }}
        placeholder="Search brand"
      />
      {BrandData.map((brand, i) => {
        return <ButtonText text={brand.name} type={brand.type} />;
      })}

      <Typography
        variant="h3"
        children="Discount"
        color="black"
        style={{ marginTop: "1em" }}
      />

      {DiscountData.map((brand, i) => {
        return <ButtonText text={brand.name} type={brand.type} />;
      })}

      <Typography
        variant="h3"
        children="Price"
        color="black"
        style={{ marginTop: "1em" }}
      />

      {PriceData.map((brand, i) => {
        return <ButtonText text={brand.name} type={brand.type} />;
      })}

      <Typography
        variant="h3"
        children="Custom Price Range"
        color="black"
        style={{ marginBottom: "1em", marginTop: "1em" }}
      />
      <MinMaxGo />

      <Typography
        variant="h3"
        children="Colors"
        color="black"
        style={{ marginBottom: "1em", marginTop: "1em" }}
      />
      <div className={styles.colorGrid}>
        {colourData.map((color, i) => (
          <div
            style={{
              width: "50px",
              height: "32px",
              backgroundColor: color,
              border: "1px solid #E0E0E0",
            }}
          />
        ))}
      </div>
    </div>
  );
}
