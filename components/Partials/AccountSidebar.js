import React from "react";
import Image from "next/image";
import Link from "next/link";

import Typography from "../Typography/Typography";
import Button from "../Button/Button";

const leftPane = {
  backgroundColor: "#fff",
  maxHeight: "310px",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const iconsText = {
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
  cursor: "pointer",
};

const active = {
  backgroundColor: "#F1F4F4",
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
  padding: ".4em 0",
  color: "#0c8dba",
  cursor: "pointer",
};

const buttonStyle = {
  width: "100%",
  backgroundColor: "#fff",
  color: "#0c8dba",
  alignSelf: "center",
  borderTop: "1px solid #E0E0E0",
  padding: "1em",
  marginTop: ".5em",
};

export default function AccountSidebar({ activeStyleIndex }) {
  return (
    <div style={leftPane}>
      <Link href="/account">
        <div style={activeStyleIndex == 1 ? active : iconsText}>
          <Image src="/assets/account.png" width="16px" height="21px" />
          <Typography
            variant="h6"
            color="black"
            children="My Account"
            style={{ marginLeft: "1em" }}
          />
        </div>
      </Link>
      <Link href="/myOrder">
        <div style={activeStyleIndex == 2 ? active : iconsText}>
          <Image src="/assets/box.png" width="16px" height="21px" />
          <Typography
            variant="h6"
            color="black"
            children="My Orders"
            style={{ marginLeft: "1em" }}
          />
        </div>
      </Link>
      <Link href="/loyalty">
        <div style={activeStyleIndex == 3 ? active : iconsText}>
          <Image src="/assets/gift.png" width="16px" height="21px" />
          <Typography
            variant="h6"
            color="black"
            children="Loyalty Point"
            style={{ marginLeft: "1em" }}
          />
        </div>
      </Link>
      <div style={activeStyleIndex == 4 ? active : iconsText}>
        <Image src="/assets/wishlist.png" width="16px" height="21px" />
        <Typography
          variant="h6"
          color="black"
          children="My Wishlist"
          style={{ marginLeft: "1em" }}
        />
      </div>
      <Button text="Logout" style={buttonStyle} />
    </div>
  );
}
