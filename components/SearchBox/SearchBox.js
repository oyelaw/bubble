import React from "react";
import Image from "next/image";

import SelectInput from "../SelectInput/SelectInput";
import Button from "../Button/Button";

import styles from "./SearchBox.module.css";
import { Classnames } from "../../utils";

const OPTIONS = [
  { value: "All categories", label: "All categories" },
  { value: "toys", label: "toys" },
];
export default function SearchBox({ className }) {
  return (
    <div className={Classnames(styles.container, className)}>
      <SelectInput options={OPTIONS} />
      <input
        type="text"
        placeholder="Search for products"
        name="search"
        className={styles.input}
      />

      <Button
        text={
          <Image src="/assets/search.png" alt="me" width="60" height="62" />
        }
        style={{ backgroundColor: "#ba390c" }}
      />
    </div>
  );
}
