import React from "react";
import dynamic from "next/dynamic";

const Dropdown = dynamic(
  async () => {
    const module = await import("reactjs-dropdown-component");
    const DD = module.Dropdown;

    return ({ forwardedRef, ...props }) => <DD ref={forwardedRef} {...props} />;
  },
  { ssr: false }
);

const Locations = [
  {
    label: "New York",
    value: "newYork",
  },
  {
    label: "Dublin",
    value: "dublin",
  },
  {
    label: "Istanbul",
    value: "istanbul",
  },
  {
    label: "California",
    value: "colifornia",
  },
  {
    label: "Izmir",
    value: "izmir",
  },
  {
    label: "Oslo",
    value: "oslo",
  },
];

export default function DropDown() {
  const onChange = () => console.log("changed");
  return (
    <Dropdown
      name="location"
      title="Select location"
      list={Locations}
      onChange={onChange}
      styles={{
        headerTitle: { color: "red", display: "none" },
      }}
    />
  );
}
