import React from "react";
import styles from "./H4.module.css";

export default function H4({ title }) {
  return <h4 className={styles.title}>{title}</h4>;
}
