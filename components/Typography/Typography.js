import React from "react";
import { Classnames } from "../../utils";
// import Classnames from "classnames";
import styles from "./Typography.module.css";

// Defining the HTML tag that the component will support
const variantsMapping = {
  h1: "h1",
  h2: "h2",
  h3: "h3",
  h4: "h4",
  h5: "h5",
  h6: "h6",
  subheading1: "h6",
  subheading2: "h6",
  body1: "p",
  body2: "p",
  body3: "p",
  body4: "p",
};

// Create a functional component that take
// variant: the selected html tag
// color: the selected color
// children: the node passed inside the Component
// ...props: the default attribute of the Component
const Typography = ({ variant, color, children, style, ...props }) => {
  // If the variant exists in variantsMapping, we use it.
  // Otherwise, use p tag instead.
  const Component = variant ? variantsMapping[variant] : "p";

  return (
    <Component
      className={Classnames(
        styles[`typography--variant-${variant}`],
        styles[`typography--color-${color}`]
      )}
      style={style}
      {...props}
    >
      {children}
    </Component>
  );
};

export default Typography;
