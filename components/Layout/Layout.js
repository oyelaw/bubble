import React from "react";
import Header from "./Header";
import Footer from "./Footer";

import styles from "./Layout.module.css";

export default function Layout({ children }) {
  return (
    <div className={styles.layout}>
      <Header />
      <div className={styles.children}>{children}</div>

      <Footer />
    </div>
  );
}
