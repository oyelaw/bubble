import React from "react";
import Image from "next/image";

import styles from "./Layout.module.css";
import Typography from "../Typography/Typography";

export default function Footer() {
  return (
    <footer className={styles.footer}>
      <div className={styles.footerContent}>
        <div>
          <Image src="/assets/whiteLogo.png" alt="me" width="180" height="24" />
          <Typography
            variant="body2"
            color="lightgrey"
            style={{ marginTop: "1.5em" }}
          >
            Bubble Colony is a fast baby business offering the best and largest
            collection of baby products for infants to toddlers from trusted
            local & international brands.
          </Typography>
          <div className={styles.footerSocials}>
            <a href="/" className={styles.socialLinks}>
              <Image
                src="/assets/instagram.png"
                alt="me"
                width="12.8"
                height="12.8"
              />
            </a>

            <a href="/" className={styles.socialLinks}>
              <Image
                src="/assets/twitter.png"
                alt="me"
                width="12.8"
                height="12.8"
              />
            </a>
            <a href="/" className={styles.socialLinks}>
              <Image
                src="/assets/youtube.png"
                alt="me"
                width="12.8"
                height="12.8"
              />
            </a>
          </div>
        </div>
        <div className={styles.aboutUs}>
          <Typography variant="h4" color="white" style={{ alignSelf: "start" }}>
            About Bubble Colony
          </Typography>
          <ul>
            <li>
              {" "}
              <Typography variant="body2" color="lightgrey">
                About
              </Typography>
            </li>
            <li>
              {" "}
              <Typography variant="body2" color="lightgrey">
                Contact Us
              </Typography>
            </li>
            <li>
              {" "}
              <Typography variant="body2" color="lightgrey">
                Terms & Conditions
              </Typography>
            </li>
            <li> </li>
          </ul>
        </div>

        <div className={styles.aboutUs}>
          <Typography variant="h4" color="white" style={{ alignSelf: "start" }}>
            Services
          </Typography>
          <ul>
            <li>
              {" "}
              <Typography variant="body2" color="lightgrey">
                Sell on Bubble Colony
              </Typography>
            </li>
            <li>
              {" "}
              <Typography variant="body2" color="lightgrey">
                Delivery & Returns
              </Typography>
            </li>
            <li>
              {" "}
              <Typography variant="body2" color="lightgrey">
                FAQS
              </Typography>
            </li>
          </ul>
        </div>
        <div className={styles.aboutUs}>
          <Typography variant="h4" color="white" style={{ alignSelf: "start" }}>
            Reach us
          </Typography>
          <ul className={styles.contactUsPoints}>
            <li>
              <Image
                src="/assets/Message.png"
                alt="me"
                width="20"
                height="18"
              />
              <Typography
                variant="body2"
                color="lightgrey"
                style={{ marginLeft: "0.8em" }}
              >
                hello@bubblecolony.com
              </Typography>
            </li>
            <li>
              <Image src="/assets/Mobile.png" alt="me" width="20" height="18" />
              <Typography
                variant="body2"
                color="lightgrey"
                style={{ marginLeft: "0.8em" }}
              >
                +234 876 43210
              </Typography>
            </li>
            <li>
              <Image
                src="/assets/Location.png"
                alt="me"
                width="20"
                height="18"
              />
              <Typography
                variant="body2"
                color="lightgrey"
                style={{ marginLeft: "0.8em" }}
              >
                2343Somewhere in lagos, NIgeria
              </Typography>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
}
