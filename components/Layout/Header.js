import React from "react";
import Head from "next/head";
import Image from "next/image";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";

import styles from "./Layout.module.css";
import Typography from "../Typography/Typography";
import Link from "next/link";
import Button from "../Button/Button";
import SearchBox from "../SearchBox/SearchBox";
import AccountSidebar from "../Partials/AccountSidebar";

const showDropdown = {
  height: "225px",
  width: "263px",
  backgroundColor: "white",
  position: "absolute",
  top: "5em",
  right: "0",
  marginRight: "1em",
};

const hideDropdown = {
  height: "225px",
  width: "263px",
  backgroundColor: "white",
  position: "fixed",
  top: "5em",
  right: "0",
  marginRight: "1em",
  display: "none",
};

export default function Header() {
  const [showMenu, setShowMenu] = React.useState(false);
  return (
    <header className={styles.header}>
      <Image src="/assets/logo.png" alt="me" width="246" height="32" />

      <div className={styles.logo}>
        <div className={styles.signin}>
          <Link href="/login">
            <Typography
              variant="h5"
              color="black"
              style={{ marginBottom: "0.2em", cursor: "Pointer" }}
            >
              Hello, Sign in
            </Typography>
          </Link>

          <Button
            icon={
              <FontAwesomeIcon
                icon={faAngleDown}
                style={{ color: "black", marginRight: ".5em" }}
              />
            }
            text={
              <Typography variant="h6" color="black">
                My Account
              </Typography>
            }
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "space-between",
              backgroundColor: "#fff",
              color: "black",
              padding: 0,
            }}
            // onClick={() => setShowMenu((showMenu) => !showMenu)}
          />
        </div>

        <div>
          <Image src="/assets/cart.png" alt="me" width="39" height="23" />
        </div>
      </div>
    </header>
  );
}

export function HeaderWithSearchBar() {
  return (
    <header className={styles.header}>
      <Image src="/assets/logo.png" alt="me" width="246" height="32" />

      <SearchBox className={styles.searchBoxBorder} />
      <div className={styles.logo}>
        <div className={styles.signin}>
          <Link href="/login">
            <Typography
              variant="h5"
              color="black"
              style={{ marginBottom: "0.2em", cursor: "Pointer" }}
            >
              Hello, Sign in
            </Typography>
          </Link>

          <Button
            icon={
              <FontAwesomeIcon
                icon={faAngleDown}
                style={{ color: "black", marginRight: ".5em" }}
              />
            }
            text={
              <Typography variant="h6" color="black">
                My Account
              </Typography>
            }
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "space-between",
              backgroundColor: "#fff",
              color: "black",
              padding: 0,
            }}
          />
        </div>

        <div>
          <Image src="/assets/cart.png" alt="me" width="39" height="23" />
        </div>
      </div>
    </header>
  );
}

export function HeaderWithoutSignin({ register, login }) {
  return (
    <header className={styles.header}>
      <Image src="/assets/logo.png" alt="me" width="246" height="32" />
      {register && (
        <Button
          text="Login"
          style={{
            backgroundColor: "white",
            color: "#0c8dba",
            justifyContent: "center",
            alignContent: "center",
            width: "222px",
            padding: "1em",
            border: "1px solid #0c8dba",
          }}
        />
      )}
      {login && (
        <Button
          text="Create Account"
          style={{
            backgroundColor: "white",
            color: "#0c8dba",
            justifyContent: "center",
            alignContent: "center",
            width: "222px",
            padding: "1em",
            border: "1px solid #0c8dba",
          }}
        />
      )}
    </header>
  );
}
