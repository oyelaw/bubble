import React from "react";
import ProductDetail from "../ProductDetail/ProductDetail";

import dynamic from "next/dynamic";
const Carousel = dynamic(() => import("@brainhubeu/react-carousel"), {
  ssr: false,
});

import "@brainhubeu/react-carousel/lib/style.css";
import { slidesToShowPlugin, autoplayPlugin } from "@brainhubeu/react-carousel";

const marginLeft = { marginLeft: "1em" };
export const ClothesCarousel = () => {
  const [value, setValue] = React.useState(0);

  function onChange(value) {
    return setValue(value);
  }
  return (
    <Carousel
      plugins={[
        "infinite",
        {
          resolve: slidesToShowPlugin,
          options: {
            numberOfSlides: 5,
          },
        },
        {
          resolve: autoplayPlugin,
          options: {
            interval: 1000,
          },
        },
      ]}
      breakpoints={{
        1200: {
          slidesPerPage: 2,
        },
        600: {
          slidesPerPage: 2,
        },
      }}
      animationSpeed={1000}
      value={value}
      onChange={onChange}
    >
      {[1, 2, 3, 4, 5, 6, 7].map((item, i) => (
        <ProductDetail
          key={i}
          image="/assets/product5.png"
          description="Bips and Roses: Zoey and Sassafras"
          style={{ marginLeft: "1.5em" }}
        />
      ))}
    </Carousel>
  );
};

export const BooksCarousel = () => {
  const [value, setValue] = React.useState(0);

  function onChange(value) {
    return setValue(value);
  }
  return (
    <Carousel
      plugins={[
        "infinite",
        {
          resolve: slidesToShowPlugin,
          options: {
            numberOfSlides: 5,
          },
        },
        {
          resolve: autoplayPlugin,
          options: {
            interval: 1000,
          },
        },
      ]}
      animationSpeed={1000}
      value={value}
      onChange={onChange}
    >
      {[1, 2, 3, 4, 5, 6, 7].map((item, i) => (
        <ProductDetail
          key={i}
          image="/assets/book1.png"
          description="Bips and Roses: Zoey and Sassafras"
          style={{ marginLeft: "1.5em" }}
        />
      ))}
    </Carousel>
  );
};
