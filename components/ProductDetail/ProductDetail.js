import React from "react";
import Link from "next/link";
import Image from "next/image";
import ReactStars from "react-rating-stars-component";

import styles from "./ProductDetail.module.css";
import Typography from "../Typography/Typography";

export default function ProductDetail({
  image,
  description,
  showHeart,
  showAddcart,
  showPrice,
  price,
  width,
  height,
  style,
}) {
  const ratingChanged = (newRating) => {
    console.log(newRating);
  };

  return (
    <Link href="/product">
      <div className={styles.galleryBox} style={style}>
        <div className={styles.topRight}>
          <Image src="/assets/heart.png" alt="me" width="20" height="20" />
        </div>

        {/* <div style={{ flex: "2" }}> */}
        <img src={image} alt="me" width="100%" height="100%" />
        {/* </div> */}

        {/* <div style={{ flex: "3" }}> */}
        <Typography
          variant="h6"
          color="black"
          children={description}
          style={{ textAlign: "left" }}
        />
        {/* </div> */}
        {showPrice && (
          <div className={styles.ratingAndPrice}>
            <div style={{ display: "flex" }}>
              <ReactStars
                count={5}
                onChange={ratingChanged}
                size={12}
                activeColor="#ffd700"
                value={3}
              />
              <Typography
                variant="body4"
                color="grey"
                style={{ marginLeft: ".4em" }}
              >
                (32 reviews)
              </Typography>
            </div>

            <h5
              style={{
                fontWeight: "800",
                margin: 0,
                marginTop: ".5em",
              }}
            ></h5>

            <Typography variant="h3" color="black">
              {price}
            </Typography>
          </div>
        )}

        {showAddcart && (
          <button className={styles.cartButton}>ADD TO CART</button>
        )}
      </div>
    </Link>
  );
}

const containerStyle = {
  display: "flex",
  flexDirection: "column",
  /* justify-content: space-between; */
  backgroundColor: "#ffffff",
  borderRadius: "8px",
  position: "relative",
  height: "100%",
};

const topRightStyle = {
  position: "absolute",
  top: "8px",
  right: "16px",
  flex: "1",
};
export function ProductDetailWithoutButton({ image, description }) {
  return (
    <Link href="/product">
      <div style={containerStyle}>
        <div style={topRightStyle}>
          <img src="/assets/heart.png" alt="me" width="100%" height="100%" />
        </div>

        <img
          src={image}
          alt="me"
          width="90%"
          height="90%"
          style={{ flex: "1" }}
        />

        <div style={{ flex: "1" }}>
          <Typography
            variant="h6"
            color="black"
            children={description}
            style={{ textAlign: "left" }}
          />
        </div>
      </div>
    </Link>
  );
}
