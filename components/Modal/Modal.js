import React from "react";

import Typography from "../Typography/Typography";
import styles from "./Modal.module.css";
import Button from "../Button/Button";

export default function Modal({ onClose, child }) {
  return (
    <div id="myModal" className={styles.modal}>
      <div className={styles.modalContent}>{child}</div>
    </div>
  );
}

export function SelectAddress({ onClose }) {
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <Typography variant="h3" color="black">
          Checkout
        </Typography>

        <Button
          text="X"
          onClick={onClose}
          style={{
            backgroundColor: "white",
            height: "30px",
            width: "30px",
            color: "#0c8dba",
            justifySelf: "right",
            fontSize: "1.5rem",
          }}
        />
      </div>
      <Typography
        variant="h3"
        color="grey"
        style={{ marginBottom: "0.625em", marginTop: "1em" }}
      >
        Default Address
      </Typography>
      <div
        style={{
          display: "flex",
        }}
      >
        <div className={styles.currentAddress}>
          <div style={{ display: "flex" }}>
            <div style={{ flex: "1" }}>
              <input
                type="radio"
                className={styles.input}
                checked
                name="address"
              />{" "}
            </div>
            <div style={{ flex: "4" }}>
              <Typography
                variant="h3"
                color="black"
                style={{ marginBottom: "0.625em" }}
              >
                Ifeanyi Umunnakwe
              </Typography>
              <Typography
                variant="body2"
                color="black"
                style={{ marginBottom: "1em", maxWidth: "500px" }}
              >
                Quits Aviation Services Free Zone Murtala Muhammed International
                Airport 23401, LagosQuits Aviation Services Free Zone Murtala
                Muhammed International Airport 23401, Lagos
              </Typography>
              <Typography
                variant="h5"
                color="black"
                style={{ marginBottom: "0.875em" }}
              >
                Phone number:
              </Typography>
              <Typography
                variant="body3"
                color="black"
                style={{ marginBottom: "0.875em" }}
              >
                080454395930
              </Typography>

              <div
                style={{
                  marginTop: "1em",
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <Typography
                  children="Remove"
                  variant="h6"
                  style={{ color: "#ba390c", marginRight: "1em" }}
                />{" "}
                |
                <Typography
                  children="Edit"
                  variant="h6"
                  style={{
                    color: "#0c8dba",
                    marginLeft: "1em",
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <Typography
        variant="h3"
        color="grey"
        style={{ marginBottom: "0.625em", marginTop: "1em" }}
      >
        Saved Address
      </Typography>

      <div className={styles.address}>
        <div style={{ display: "flex" }}>
          <div style={{ flex: "1" }}>
            <input type="radio" className={styles.input} name="address" />{" "}
          </div>

          <div style={{ flex: "4" }}>
            <Typography
              variant="h3"
              color="black"
              style={{ marginBottom: "0.625em" }}
            >
              Ifeanyi Umunnakwe
            </Typography>
            <Typography
              variant="body2"
              color="black"
              style={{ marginBottom: "1em", maxWidth: "500px" }}
            >
              Quits Aviation Services Free Zone Murtala Muhammed International
              Airport 23401, LagosQuits Aviation Services Free Zone Murtala
              Muhammed International Airport 23401, Lagos
            </Typography>
            <Typography
              variant="h5"
              color="black"
              style={{ marginBottom: "0.875em" }}
            >
              Phone number:
            </Typography>
            <Typography
              variant="body3"
              color="black"
              style={{ marginBottom: "0.875em" }}
            >
              080454395930
            </Typography>

            <div
              style={{
                marginTop: "1em",
                display: "flex",
                alignItems: "center",
              }}
            >
              <Typography
                children="Remove"
                variant="h6"
                style={{ color: "#ba390c", marginRight: "1em" }}
              />{" "}
              |
              <Typography
                children="Edit"
                variant="h6"
                style={{
                  color: "#0c8dba",
                  marginLeft: "1em",
                }}
              />
            </div>
          </div>
        </div>
      </div>

      <Button
        text="Use this Address"
        style={{ width: "100%", height: "48px", margin: "2em 0" }}
      />
    </div>
  );
}
