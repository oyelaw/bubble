import React from "react";
import styles from "./SelectInput.module.css";
import Typography from "../Typography/Typography";

import { Classnames } from "../../utils";

export default function SelectInput({ options, style, label }) {
  return (
    <div className={styles.selecta} style={style}>
      <select id="standard-select">
        {options.map((option, i) => {
          return <option value={option.value}>{option.label}</option>;
        })}
      </select>
    </div>
  );
}

export function TextInput({
  placeholder,
  value,
  onChangeText,
  className,
  onChange,
  style,
  label,
}) {
  return (
    // TODO ADD SUPPORT FOR LABELS

    <input
      placeholder={placeholder}
      onChangeText={onChangeText}
      onChange={onChange}
      value={value}
      className={Classnames(styles.textInput, className)}
      style={style}
    />
  );
}

export function TextArea({
  placeholder,
  value,
  onValueChange,
  className,
  style,
  row,
}) {
  return (
    <textarea
      placeholder={placeholder}
      onValueChange={onValueChange}
      value={value}
      className={Classnames(styles.textInput, className)}
      style={style}
      rows={row}
    />
  );
}

export function SwitchComponent({ onClick }) {
  return (
    <label className={styles.switch}>
      <input type="checkbox" onClick={onClick} />
      <span className={Classnames(styles.slider, styles.round)}></span>
    </label>
  );
}
