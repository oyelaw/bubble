import React from "react";
import Image from "next/image";
import * as Yup from "yup";

import Typography from "../components/Typography/Typography";
import Button, { GoBack } from "../components/Button/Button";
import AccountSidebar from "../components/Partials/AccountSidebar";
import Table from "../components/Table/Table";

const schema = Yup.object().shape({
  address: Yup.string().required("Please type your address").trim(),
  city: Yup.string().required("Please type your city").trim(),
  state: Yup.string().required("Please select your state").trim(),
  fullname: Yup.string().required("Please type your fullname").trim(),
  phoneNumber: Yup.string().required("Please type your phone number").trim(),
});

const containerStyle = {
  backgroundColor: "#E5E5E5",
  display: "grid",
  gridTemplateColumns: "1fr 4fr",
  gridGap: "1em",
  padding: "1.7em",
};

const leftPane = {
  backgroundColor: "#fff",
  maxHeight: "310px",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const rightPane = {
  backgroundColor: "#fff",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const iconsText = {
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
};

const active = {
  backgroundColor: "#F1F4F4",
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
  padding: ".4em",
};

const buttonStyle = {
  width: "100%",
  backgroundColor: "#fff",
  color: "#0c8dba",
  alignSelf: "center",
  borderTop: "1px solid #E0E0E0",
  padding: "1em",
  marginTop: ".5em",
};

export default function Child() {
  return (
    <div style={containerStyle}>
      <AccountSidebar activeStyleIndex={3} />
      <div>
        <GoBack />
        <div style={rightPane}>
          <Typography
            children="Loyalty Point"
            variant="h3"
            color="black"
            style={{ marginBottom: "1em" }}
          />

          <div
            style={{
              display: "flex",
              justifyContent: "space-evenly",
              borderTop: "1px solid #595F62",
              padding: "1em",
            }}
          >
            <div
              style={{
                minWidth: "208px",
                backgroundColor: "#F8F9F9",
                justifyContent: "center",
                alignContent: "center",
                justifyItems: "center",
                alignItems: "center",
                display: "flex",
                flexDirection: "column",
                marginRight: "2em",
              }}
            >
              <Image src="/assets/ownership.png" width="62" height="59" />
              <Typography
                children="50034"
                variant="h3"
                color="blue"
                style={{ margin: "1em 0" }}
              />

              <Typography
                children="Loyalty Balance"
                variant="body3"
                color="grey"
                style={{ margin: ".1em 0" }}
              />

              <Typography
                children="( 1 Loyalty Point = N1)"
                variant="body3"
                color="black"
                style={{ margin: ".1em 0" }}
              />
            </div>
            <div
              style={{
                width: "100%",
                backgroundColor: "#F8F9F9",
                display: "flex",
                padding: "1em",
              }}
            >
              <Image src="/assets/kidShopping.png" width="168" height="168" />
              <div style={{ marginLeft: "1em" }}>
                <Typography
                  children="Get loyalty point when you shop"
                  variant="h3"
                  color="black"
                  style={{ marginBottom: "1.25em" }}
                />

                <Typography
                  children="Get 1 loyalty point for the every N200 spent while shopping."
                  variant="body3"
                  color="grey"
                  style={{ marginBottom: "2em", width: "330px" }}
                />

                <Button
                  text="Start Shopping"
                  style={{
                    backgroundColor: "#fff",
                    color: "#0C8DBA",
                    width: "197px",
                    padding: "1em",
                    border: "1px solid #0C8DBA",
                  }}
                />
              </div>
            </div>
          </div>

          <Typography
            children="Loyalty Point History"
            variant="h3"
            color="black"
            style={{ margin: "1em 0" }}
          />

          <Table />
        </div>
      </div>
    </div>
  );
}
