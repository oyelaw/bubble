import React from "react";

import Typography from "../components/Typography/Typography";
import Table from "../components/Table/Table";
import AccountSidebar from "../components/Partials/AccountSidebar";

const containerStyle = {
  backgroundColor: "#E5E5E5",
  display: "grid",
  gridTemplateColumns: "1fr 4fr",
  gridGap: "1em",
  padding: "1.7em",
  flex: 1,
};

const rightPane = {
  backgroundColor: "#fff",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

export default function Account() {
  const [showTable, setTable] = React.useState(null);

  const [data, setData] = React.useState([1, 2, 3, 4]);

  const handleRowClick = (i) => {
    if (i === showTable) {
      setTable(null);
      return;
    }

    return setTable(i);
  };
  return (
    <div style={containerStyle}>
      <AccountSidebar />
      <div style={rightPane}>
        <Typography
          children="Children Profile"
          variant="h3"
          color="black"
          style={{ marginBottom: "1em" }}
        />
        <div
          style={{
            display: "flex",
            borderTop: "1px solid #E0E0E0",
            paddingTop: "1em",
            flexDirection: "column",
            minHeight: "500px",
          }}
        >
          {data.map((item, i) => {
            return (
              <div
                style={{
                  borderTop: "3px solid #0C8DBA",
                  backgroundColor: "#F8F9F9",
                  marginBottom: "2em",
                }}
              >
                <div
                  style={{
                    padding: "1em",
                    display: "grid",
                    gridTemplateColumns: "11fr 1fr",
                    gridGap: "1em",
                  }}
                >
                  <div
                    style={{
                      display: "grid",
                      gridTemplateColumns: "1fr 1fr 1fr",
                      gridGap: "2em",
                    }}
                    onClick={() => handleRowClick(i)}
                  >
                    <div>
                      <Typography variant="body4" children="Child's Name" />
                      <Typography variant="h3" children="Okiki Lawrence" />
                    </div>

                    <div>
                      <Typography variant="body4" children="Age" />
                      <Typography variant="h3" children="12" />
                    </div>

                    <div>
                      <Typography variant="body4" children="Gender" />
                      <Typography variant="h3" children="Male" />
                    </div>

                    <div>
                      <Typography variant="body4" children="Bank Assigned" />
                      <Typography variant="h3" children="Sterling Bank" />
                    </div>
                    <div>
                      <Typography
                        variant="body4"
                        children="Total Naira Point Value"
                      />
                      <Typography variant="h3" children="₦14,000" />
                    </div>
                  </div>
                  <div
                    style={{
                      justifyContent: "space-between",
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      padding: "1em",
                      backgroundColor: " #DC3545",
                      borderRadius: "12px",
                      opacity: "0.6",
                      cursor: "pointer",
                      zIndex: "7",
                    }}
                    onClick={(i) => {
                      const dte = [...data];
                      dte.splice(i, 1);
                      setData(dte);
                    }}
                  >
                    <Typography variant="h3" children="X" color="white" />

                    <Typography variant="h3" children="delete" color="white" />
                  </div>
                </div>
                <div
                  style={{
                    marginTop: "1em",
                    paddingTop: "1em",
                    borderTop: "3px solid rgba(0, 0, 0, 0.125)",
                    display: `${i === showTable ? "block" : "none"}`,
                  }}
                >
                  <Table />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
