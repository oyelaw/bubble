import React, { useState } from "react";
import Image from "next/image";

import styles from "../styles/NewAddress.module.css";
import { LayoutWithSearchBarInHeader } from "../components/Layout/PublicLayout";
import Typography from "../components/Typography/Typography";
import Modal, { SelectAddress } from "../components/Modal/Modal";
import Button from "../components/Button/Button";
import { Classnames } from "../utils";
import Link from "next/link";

const borderTopAndBottom = {
  borderTop: "1px solid #f6f6f6",
  borderBottom: "1px solid #f6f6f6",
  padding: "1em",
};

function Checkout() {
  const [showModal, setShowModal] = useState(false);

  const onClose = () => setShowModal(false);
  return (
    <div className={styles.container}>
      {showModal && (
        <Modal
          onClose={onClose}
          child={<SelectAddress onClose={() => onClose()} />}
        />
      )}

      <div className={styles.header} onClick={() => setShowModal(false)}>
        <Typography children="Checkout" variant="h3" color="black" />
        <div style={{ display: "flex", alignItems: "center" }}>
          <div className={styles.circle}>
            <Image src="/assets/step1.png" width="16" height="16" />
          </div>
          <div className={styles.text}>Delivery Address</div>
          <div className={styles.dash}>
            <Image src="/assets/greyDash.png" width="16" height="3.48" />
          </div>
          <div className={Classnames(styles.circle)}>
            {" "}
            <Image src="/assets/step2.png" width="16" height="16" />
          </div>
          <div className={styles.text}>Payment Method</div>
        </div>
      </div>

      <div className={styles.addressAndSummary}>
        <div style={{ flex: "4" }}>
          <div className={styles.form}>
            <Typography
              children="Delivery Address"
              variant="h3"
              color="black"
              style={{ marginBottom: "1em" }}
            />
            <div className={styles.formContainer}>
              <div
                className={Classnames(styles.currentAddress, styles.addAddress)}
                onClick={() => setShowModal(true)}
                style={{ flex: "1", height: "inherit", maxHeight: "265px" }}
              >
                <Image
                  src="/assets/location1.png"
                  alt="me"
                  width="100%"
                  height="100%"
                />
                <Typography
                  children="Add New Address"
                  variant="h3"
                  color="black"
                  style={{ marginBottom: "1em" }}
                />
              </div>
              <div style={{ flex: "1", marginLeft: "1em" }}>
                <div className={styles.currentAddress}>
                  <div className={styles.topRight}>
                    <Image
                      src="/assets/check.png"
                      alt="me"
                      width="9"
                      height="9"
                    />
                  </div>
                  <Typography
                    variant="h3"
                    color="black"
                    style={{ marginBottom: "0.625em" }}
                  >
                    Ifeanyi Umunnakwe
                  </Typography>
                  <Typography
                    variant="body2"
                    color="black"
                    style={{ marginBottom: "1em" }}
                  >
                    Quits Aviation Services Free Zone Murtala Muhammed
                    International Airport 23401, LagosQuits Aviation Services
                    Free Zone Murtala Muhammed International Airport 23401,
                    Lagos
                  </Typography>
                  <Typography
                    variant="h5"
                    color="black"
                    style={{ marginBottom: "0.875em" }}
                  >
                    Phone number:
                  </Typography>
                  <Typography
                    variant="body3"
                    color="black"
                    style={{ marginBottom: "0.875em" }}
                  >
                    080454395930
                  </Typography>

                  <div
                    style={{
                      marginTop: "1em",
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Typography
                      children="Remove"
                      variant="h6"
                      style={{ color: "#ba390c", marginRight: "1em" }}
                    />{" "}
                    |
                    <Typography
                      children="Edit"
                      variant="h6"
                      style={{
                        color: "#0c8dba",
                        marginLeft: "1em",
                        marginRight: "1em",
                      }}
                    />
                    |
                    <Typography
                      children="Change Address"
                      variant="h6"
                      style={{ color: "#0c8dba", marginLeft: "1em" }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <Link href="paymentMethod">
              <Button
                text="Continue to Payment"
                style={{
                  height: "48px",
                  width: "100%",
                  margin: "2.5em 0 1.5em",
                  backgroundColor: "#0c8dba",
                  color: "#ffffff",
                  alignSelf: "center",
                }}
              />
            </Link>
          </div>
        </div>

        <div style={{ flex: "1", marginLeft: "1em" }}>
          <div className={styles.rightCard}>
            <Typography
              children="Order Summary"
              variant="h3"
              color="black"
              style={{ marginBottom: "1em" }}
            />
            <div style={borderTopAndBottom}>
              <div className={styles.orderDetail}>
                <Typography variant="body2" children="Items(2):" />
                <Typography variant="body2" children="N29,500.00" />
              </div>
              <div className={styles.orderDetail}>
                <Typography variant="body2" children="Order Discounts:" />
                <Typography
                  variant="body2"
                  children="N29,500.00"
                  style={{ color: "red" }}
                />
              </div>
            </div>

            <div className={styles.orderDetail} style={borderTopAndBottom}>
              <Typography
                variant="body2"
                children=" Estimated Shipping:"
                style={{ margin: "2.31em 0" }}
              />

              <Typography
                variant="body2"
                children="  Add your Delivery address at checkout to see delivery charges"
                style={{ margin: "2.31em 0", maxWidth: "132px" }}
              />
            </div>

            <div className={styles.orderDetail} style={borderTopAndBottom}>
              <Typography variant="body2" children="Estimated Total:" />
              <Typography variant="body2" children="N28, 000.00" />
            </div>

            <div className={styles.icons}>
              <span className={styles.accept}>we accept</span>
              <Image src="/assets/mastercard.png" width="21" height="15" />
              <Image src="/assets/visa.png" width="47" height="15" />
              <Image src="/assets/verve.png" width="38" height="15" />
              <Image src="/assets/paga.png" width="53" height="15" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

Checkout.getLayout = (page) => (
  <LayoutWithSearchBarInHeader>{page}</LayoutWithSearchBarInHeader>
);

export default Checkout;
