import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { Formik } from "formik";
import * as Yup from "yup";

import styles from "../styles/PaymentMethod.module.css";
import { LayoutWithSearchBarInHeader } from "../components/Layout/PublicLayout";
import Typography from "../components/Typography/Typography";
import Modal from "../components/Modal/Modal";
import SelectInput, { TextInput } from "../components/SelectInput/SelectInput";
import Button from "../components/Button/Button";
import { Classnames } from "../utils";

const schema = Yup.object().shape({
  child: Yup.string().required("Please type the name on card").trim(),
});

export function SelectChildForm({ onClose }) {
  return (
    <div className={styles.selectChildForm}>
      <Formik
        initialValues={{
          child: "",
        }}
        validationSchema={schema}
        enableReinitialize={true}
        onSubmit={(values) => {
          console.log(values);
        }}
      >
        {(props) => {
          const { handleChange, values, handleSubmit, errors, touched } = props;

          return (
            <div className={styles.form}>
              <div
                style={{
                  borderBottom: "1px solid #E0E0E0",
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Typography
                  variant="h3"
                  color="black"
                  style={{
                    padding: "1em 0",
                  }}
                >
                  Which child are you buying for?
                </Typography>

                <Button
                  text="X"
                  style={{
                    padding: "1em 0",
                    border: "none",
                    backgroundColor: "#fff",
                    color: "#0C8DBA",
                    fontSize: "1.5rem",
                  }}
                  onClick={onClose}
                />
              </div>

              <Typography
                variant="body2"
                color="black"
                style={{ marginTop: "1em" }}
              >
                Gain some credit points when you buy products for your child
              </Typography>

              <Typography
                variant="body3"
                color="black"
                style={{ margin: "1em 0 .5em 0" }}
              >
                Select Child
              </Typography>

              <SelectInput
                placeholder="Select Child"
                type="select"
                onChangeText={handleChange("child")}
                value={values.cardNumber}
                options={[
                  { value: "Taiwo", label: "Taiwo" },
                  { value: "Kehinde", label: "Kehinde" },
                ]}
                style={{
                  width: "100%",
                  backgroundColor: " rgba(12, 141, 186, 0.02)",
                  border: "1px solid #E0E0E0",
                  height: "48px",
                }}
              />
              <Button
                text="Select Child"
                style={{ width: "549px", height: "48px", marginTop: "1.75em" }}
              />
            </div>
          );
        }}
      </Formik>
    </div>
  );
}

const borderTopAndBottom = {
  borderTop: "1px solid #f6f6f6",
  borderBottom: "1px solid #f6f6f6",
  padding: "1em",
};

function Checkout() {
  const [showModal, setShowModal] = useState(false);

  const onClose = () => setShowModal(false);

  return (
    <div className={styles.container}>
      {showModal && (
        <Modal
          onClose={onClose}
          child={<SelectChildForm onClose={onClose} />}
        />
      )}
      <div className={styles.header}>
        <Typography children="Checkout" variant="h3" color="black" />
        <div style={{ display: "flex", alignItems: "center" }}>
          <div className={styles.circle}>
            <Image src="/assets/step1.png" width="16" height="16" />
          </div>
          <div className={styles.text}>Delivery Address</div>
          <div className={styles.dash}>
            <Image src="/assets/greyDash.png" width="16" height="3.48" />
          </div>
          <div className={Classnames(styles.circle)}>
            {" "}
            <Image src="/assets/step22.png" width="16" height="16" />
          </div>
          <div className={styles.text}>Payment Method</div>
        </div>
      </div>

      <div className={styles.addressAndSummary}>
        <div style={{ flex: "4" }}>
          <div className={styles.form}>
            <Typography
              children="Payment Method"
              variant="h3"
              color="black"
              style={{ marginBottom: "1em" }}
            />
            <div className={styles.sub}>
              <Typography
                children="Select Payment Method"
                variant="h5"
                color="black"
                style={{ margin: "1em 0" }}
              />
              <div className={styles.methods}>
                <div className={styles.method}>
                  <div
                    style={{
                      display: "flex",
                      padding: "1em",
                      borderBottom: "1px solid rgba(89, 95, 98, 0.2)",
                      alignItems: "center",
                    }}
                    onClick={() => setShowModal(true)}
                  >
                    <input type="radio" name="pay" />
                    <Typography
                      children="Pay now"
                      variant="h5"
                      color="black"
                      style={{ margin: "0 0 0 .5em" }}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "flex-start",
                      padding: "2em",
                    }}
                  >
                    <Image
                      src="/assets/mastercard.png"
                      width="40"
                      height="16"
                    />
                    <Image src="/assets/visa.png" width="40" height="16" />
                    <Image src="/assets/verve.png" width="40" height="16" />
                  </div>
                </div>

                <div className={styles.inactive}>
                  <div
                    style={{
                      display: "flex",
                      padding: "1em",
                      alignItems: "center",
                      borderBottom: "1px solid rgba(89, 95, 98, 0.2)",
                    }}
                  >
                    <input type="radio" name="pay" />
                    <Typography
                      children="Pay on Delivery"
                      variant="h5"
                      color="black"
                      style={{ margin: "0 0 0 .5em" }}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "flex-start",
                      padding: "1em",
                    }}
                  >
                    <Typography
                      children=" Pay with cash or POS on delivery"
                      variant="body3"
                      color="black"
                      style={{ margin: "0 0 0 .5em", color: "#595F62" }}
                    />
                  </div>
                </div>

                <div className={styles.inactive}>
                  <div
                    style={{
                      display: "flex",
                      padding: "1em",
                      alignItems: "center",
                      borderBottom: "1px solid rgba(89, 95, 98, 0.2)",
                    }}
                  >
                    <input type="radio" name="pay" />
                    <Typography
                      children="Pay with Loyalty Points"
                      variant="h5"
                      color="black"
                      style={{ margin: "0 0 0 .5em" }}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "flex-start",
                      padding: "1em",
                    }}
                  >
                    <Typography
                      children="You have a balance of 50034 bubble colony loyalty point."
                      variant="body3"
                      color="black"
                      style={{ margin: "0 0 0 .5em", color: "#595F62" }}
                    />
                  </div>
                </div>
              </div>
            </div>

            <Typography
              children="Payment Details"
              variant="h3"
              color="black"
              style={{ margin: "1em 0" }}
            />
            {/* START FORM */}

            <div>
              <Formik
                initialValues={{
                  name: "",
                  cardNumber: "",
                  cvv: "",
                  expiryDate: "",
                }}
                validationSchema={schema}
                enableReinitialize={true}
                onSubmit={(values) => {
                  console.log(values);
                }}
              >
                {(props) => {
                  const {
                    handleChange,
                    values,
                    handleSubmit,
                    errors,
                    touched,
                  } = props;

                  return (
                    <div style={{ display: "flex", flexDirection: "column" }}>
                      <TextInput
                        placeholder="Name on card"
                        onChangeText={handleChange("name")}
                        value={values.name}
                        style={{ width: "100%" }}
                      />
                      <TextInput
                        placeholder="Card Number"
                        onChangeText={handleChange("cradNumber")}
                        value={values.cardNumber}
                        style={{ width: "100%" }}
                      />

                      <div style={{ display: "flex" }}>
                        {" "}
                        <TextInput
                          placeholder="CVV"
                          onChangeText={handleChange("cvv")}
                          value={values.cvv}
                          style={{ width: "50%" }}
                        />
                        <TextInput
                          placeholder="Expiry Date"
                          onChangeText={handleChange("expiryDate")}
                          value={values.expiryDate}
                          style={{ marginLeft: ".5em", width: "50%" }}
                        />
                      </div>

                      <Link href="/success">
                        <Button
                          text="Pay"
                          style={{
                            width: "100%",
                            height: "48px",
                            margin: "2em 0",
                          }}
                        />
                      </Link>
                    </div>
                  );
                }}
              </Formik>
            </div>

            {/* END FORM */}
          </div>
        </div>
        <div style={{ flex: "1", marginLeft: "1em" }}>
          <div className={styles.rightCard}>
            <Typography
              children="Order Summary"
              variant="h3"
              color="black"
              style={{ marginBottom: "1em" }}
            />
            <div style={borderTopAndBottom}>
              <div className={styles.orderDetail}>
                <Typography variant="body2" children="Items(2):" />
                <Typography variant="body2" children="N29,500.00" />
              </div>
              <div className={styles.orderDetail}>
                <Typography variant="body2" children="Order Discounts:" />
                <Typography
                  variant="body2"
                  children="N29,500.00"
                  style={{ color: "red" }}
                />
              </div>
            </div>

            <div className={styles.orderDetail} style={borderTopAndBottom}>
              <Typography
                variant="body2"
                children=" Estimated Shipping:"
                style={{ margin: "2.31em 0" }}
              />

              <Typography
                variant="body2"
                children="  Add your Delivery address at checkout to see delivery charges"
                style={{ margin: "2.31em 0", maxWidth: "132px" }}
              />
            </div>

            <div className={styles.orderDetail} style={borderTopAndBottom}>
              <Typography variant="body2" children="Estimated Total:" />
              <Typography variant="body2" children="N28, 000.00" />
            </div>

            <div className={styles.icons}>
              <span className={styles.accept}>we accept</span>
              <Image src="/assets/mastercard.png" width="21" height="15" />
              <Image src="/assets/visa.png" width="47" height="15" />
              <Image src="/assets/verve.png" width="38" height="15" />
              <Image src="/assets/paga.png" width="53" height="15" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

Checkout.getLayout = (page) => (
  <LayoutWithSearchBarInHeader>{page}</LayoutWithSearchBarInHeader>
);

export default Checkout;
