import React from "react";
import Image from "next/image";

import Typography from "../components/Typography/Typography";
import Button from "../components/Button/Button";
import Table from "../components/Table/Table";
import AccountSidebar from "../components/Partials/AccountSidebar";

const containerStyle = {
  backgroundColor: "#E5E5E5",
  display: "grid",
  gridTemplateColumns: "1fr 4fr",
  gridGap: "1em",
  padding: "1.7em",
  flex: 1,
};

const leftPane = {
  backgroundColor: "#fff",
  maxHeight: "310px",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const rightPane = {
  backgroundColor: "#fff",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const iconsText = {
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
};

const active = {
  backgroundColor: "#F1F4F4",
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
  padding: ".4em",
};

const buttonStyle = {
  width: "100%",
  backgroundColor: "#fff",
  color: "#0c8dba",
  alignSelf: "center",
  borderTop: "1px solid #E0E0E0",
  padding: "1em",
  marginTop: ".5em",
};

const boxStyle = {
  height: "192px",
  background: "rgba(12, 141, 186, 0.03)",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
};

const EditButtonStyle = {
  width: "100%",
  backgroundColor: "inherit",
  color: "#0c8dba",
  textAlign: "left",
  marginTop: ".5em",
};

const profileRow = {
  display: "flex",
  justifyItems: "space-between",
};
export default function Account() {
  const [showTable, setTable] = React.useState(false);
  return (
    <div style={containerStyle}>
      <AccountSidebar />
      <div style={rightPane}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <Typography
            children="My Order"
            variant="h3"
            color="black"
            style={{ marginBottom: "1em" }}
          />

          <Button
            text="All Orders"
            style={{ marginBottom: "1em", width: "134px" }}
          />
        </div>

        <div
          style={{
            display: "flex",
            borderTop: "1px solid #E0E0E0",
            paddingTop: "1em",
            flexDirection: "column",
            minHeight: "500px",
          }}
        >
          {[1].map((item, i) => {
            return (
              <div
                onClick={() => setTable((showTable) => !showTable)}
                style={{
                  borderTop: "1px solid #0C8DBA",
                  backgroundColor: "#F8F9F9",
                  marginBottom: "2em",
                }}
              >
                <div
                  style={{
                    padding: "1em",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      borderRadius: "4px",
                      alignContent: "flex-end",
                      justifyContent: "space-between",
                      alignItems: "center",
                      borderBottom: "1px solid grey",
                      padding: "0 0 1em 0",
                      marginBottom: "2em",
                    }}
                  >
                    <Typography
                      variant="h3"
                      children="Order #1324"
                      color="black"
                      style={{ padding: ".5em" }}
                    />
                    <Typography
                      variant="body3"
                      children="Pending Delivery"
                      color="white"
                      style={{
                        padding: ".5em",
                        width: "186px",
                        backgroundColor: " #DC3545",
                        borderRadius: "8px",
                      }}
                    />
                  </div>
                  <div
                    style={{
                      display: "grid",
                      gridTemplateColumns: "1fr 1fr 1fr 1fr",
                      gridGap: "2em",
                    }}
                  >
                    <div>
                      <Typography variant="body4" children="Customer" />
                      <Typography variant="h3" children="Okiki Lawrence" />
                    </div>

                    <div>
                      <Typography variant="body4" children="Items" />
                      <Typography variant="h3" children="12 items" />
                    </div>

                    <div>
                      <Typography variant="body4" children="Price" />
                      <Typography variant="h3" children="N14, 000" />
                    </div>

                    <div>
                      <Typography variant="body4" children="Sold by" />
                      <Typography variant="h3" children="Sterling Bank" />
                    </div>
                  </div>
                </div>
                <div
                  style={{
                    marginTop: "1em",
                    paddingTop: "1em",
                    borderTop: "1px solid rgba(0, 0, 0, 0.125)",
                    display: `${showTable ? "block" : "none"}`,
                  }}
                >
                  <OrderBreakdown />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

// Move to separate file
export function OrderBreakdown() {
  return (
    <div>
      <Typography
        variant="h6"
        children="Items in your order"
        color="black"
        style={{
          padding: ".5em 0 1em .5em",
          borderBottom: "1px solid grey",
          marginBottom: "1em",
        }}
      />
      {[1, 2, 3].map((item, i) => {
        return (
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              margin: "1em 0",
            }}
          >
            <div style={{ display: "flex", alignItems: "center" }}>
              <Image src="/assets/book1.png" width="56" height="46" />
              <Typography
                variant="body3"
                children="Baby Trend Serene Nursery Center, Hello Kitty Classic Dot"
                color="black"
                style={{ maxWidth: "320px" }}
              />
            </div>

            <Typography variant="body3" children="1 Item" color="black" />

            <Typography variant="body3" children="N7,500.00" color="black" />
          </div>
        );
      })}
      <div
        style={{
          display: "grid",
          gridTemplateColumns: "1fr 1fr",
          gridGap: "1em",
        }}
      >
        <div
          style={{
            backgroundColor: "rgba(12, 141, 186, 0.03)",
            padding: "1em",
            borderRadius: "4px",
          }}
        >
          <Typography
            variant="h6"
            children="Payment Details"
            color="black"
            style={{
              padding: ".5em 0 1em .5em",
              borderBottom: "1px solid #595F62",
              marginBottom: "1em",
            }}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              margin: "1em 0",
            }}
          >
            <Typography
              variant="body3"
              children="Payment Method"
              color="black"
            />

            <Typography variant="body3" children="Card" color="black" />
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              margin: "1em 0",
            }}
          >
            <Typography variant="body3" children="Total Amount" color="black" />

            <Typography variant="body3" children="N29,000.00" color="black" />
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              margin: "1em 0",
            }}
          >
            <Typography
              variant="body3"
              children="Shipping Fees"
              color="black"
            />

            <Typography variant="body3" children="N1,000.00" color="black" />
          </div>

          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              margin: "1em 0",
            }}
          >
            <Typography variant="body3" children="Total Amount" color="black" />

            <Typography variant="body3" children="N30,000.00" color="black" />
          </div>
        </div>
        <div
          style={{
            backgroundColor: "rgba(12, 141, 186, 0.03)",
            padding: "1em",
            borderRadius: "4px",
          }}
        >
          <Typography
            variant="h6"
            children="Delivery Details"
            color="black"
            style={{
              padding: ".5em 0 1em .5em",
              borderBottom: "1px solid #595F62",
              marginBottom: "1em",
            }}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              margin: "1em 0",
            }}
          >
            <Typography
              variant="body3"
              children="Shipping Address"
              color="black"
            />

            <Typography
              variant="body3"
              children="Quits Aviation Services Free Zone Murtala Muhammed International Airport 23401, LagosQuits Aviation Services Free Zone Murtala Muhammed International Airport 23401, Lagos"
              color="black"
              style={{ maxWidth: "292px", textAlign: "right" }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
