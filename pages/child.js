import React from "react";
import Image from "next/image";
import * as Yup from "yup";

import Typography from "../components/Typography/Typography";
import Button, { GoBack } from "../components/Button/Button";
import AccountSidebar from "../components/Partials/AccountSidebar";
import Link from "next/link";

const schema = Yup.object().shape({
  address: Yup.string().required("Please type your address").trim(),
  city: Yup.string().required("Please type your city").trim(),
  state: Yup.string().required("Please select your state").trim(),
  fullname: Yup.string().required("Please type your fullname").trim(),
  phoneNumber: Yup.string().required("Please type your phone number").trim(),
});

const containerStyle = {
  backgroundColor: "#E5E5E5",
  display: "grid",
  gridTemplateColumns: "1fr 4fr",
  gridGap: "1em",
  padding: "1.7em",
};

const leftPane = {
  backgroundColor: "#fff",
  maxHeight: "310px",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const rightPane = {
  backgroundColor: "#fff",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const iconsText = {
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
};

const active = {
  backgroundColor: "#F1F4F4",
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
  padding: ".4em",
};

const buttonStyle = {
  width: "100%",
  backgroundColor: "#fff",
  color: "#0c8dba",
  alignSelf: "center",
  borderTop: "1px solid #E0E0E0",
  padding: "1em",
  marginTop: ".5em",
};

export default function Child() {
  return (
    <div style={containerStyle}>
      <AccountSidebar activeStyleIndex={1} />
      <div>
        <GoBack />
        <div style={rightPane}>
          <Typography
            children="Children Profile"
            variant="h3"
            color="black"
            style={{ marginBottom: "1em" }}
          />
          <div
            style={{
              display: "flex",
              borderTop: "1px solid #E0E0E0",
              paddingTop: "1em",
              justifyContent: "center",
              alignContent: "center",
              alignItems: "center",
              justifyItems: "center",
              flexDirection: "column",
              textAlign: "center",
            }}
          >
            <Typography variant="h3" color="grey" style={{ margin: "2.5em 0" }}>
              You haven’t added any child profile
            </Typography>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                backgroundColor: "rgba(241, 244, 244, 0.5)",
                width: "595px",
                padding: "1em",
                marginTop: "1em",
                marginBottom: "3em",
              }}
            >
              <Typography
                variant="h2"
                color="black"
                style={{ marginBottom: "1.5em" }}
              >
                Add Your Child’s Profile
              </Typography>
              <Typography
                variant="body3"
                color="grey"
                style={{
                  marginBottom: "1.25em",
                }}
              >
                Add your child’s profile and start getting credit for every
                purchase made for them
              </Typography>

              <Link href="/childProfile">
                <Button
                  text="Add your Child"
                  style={{
                    backgroundColor: "#fff",
                    color: "#0C8DBA",
                    border: "1px solid #0C8DBA",
                    padding: "1em",
                    width: "197px",
                    alignSelf: "center",
                  }}
                />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
