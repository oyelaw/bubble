import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import styles from "../styles/Home.module.css";

import SearchBox from "../components/SearchBox/SearchBox";
import {
  CardWithButton,
  CardWithGrid,
  CardImage,
} from "../components/Card/Card";
import ImageWithOverlay from "../components/Image/ImageWithOverlay";
import Typography from "../components/Typography/Typography";
import {
  ClothesCarousel,
  BooksCarousel,
} from "../components/Carousel/Carousel";
import NewsLetter from "../components/NewsLetter/NewsLetter";

const headerStyles = {
  marginTop: "2em",
  marginBottom: "1em",
  maxWidth: "712px",
};
export default function Home() {
  return (
    <main className={styles.main}>
      <Head>
        <title>Bubble Colony</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={styles.container}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
          }}
        >
          <Typography
            children="  Find great selection of kids
            clothes, baby clothes & more"
            variant="h1"
            color="black"
            style={headerStyles}
          />

          <SearchBox />
        </div>
      </div>
      <div
        style={{
          padding: "2em",
          display: "flex",
          flexDirection: "column",
        }}
      >
        {/* SAMPLE FIRST ROW */}
        <div className={styles.gridRow}>
          <CardWithButton
            title="Sign up for the best experience"
            body="Continue"
            to="/account"
          />

          <CardWithGrid title="Shop by Category" to="/category" />

          <CardImage
            title="Deals & promos"
            body={
              <img
                src="/assets/deals&promo.png"
                alt="me"
                style={{ width: "100%", height: "100%" }}
              />
            }
          />

          <CardImage
            title="Recently Viewed"
            body={
              <img
                src="/assets/recentlyViewed.png"
                alt="me"
                style={{ width: "100%", height: "100%" }}
              />
            }
          />
        </div>

        {/* END SMAPLE FIRST ROW */}

        {/* SAMPLE SECOND ROW */}
        <div className={styles.gridRow}>
          <Link href="/category">
            <ImageWithOverlay
              image="/assets/boyClothing.png"
              text="Boy clothing"
            />
          </Link>

          <ImageWithOverlay
            image="/assets/girlClothing.png"
            text="Girl clothing"
          />

          <ImageWithOverlay image="/assets/shopDenim.png" text="Shop Denim" />

          <ImageWithOverlay
            image="/assets/shoppingSummer.png"
            text="Shopping Summer"
          />
        </div>
        {/* END SAMPLE SECOND ROW */}

        <div className={styles.secondRow}>
          <Typography
            children="Top Selling Products"
            variant="h3"
            color="black"
            style={{ marginBottom: "0.8125em" }}
          />
          <ClothesCarousel />
        </div>

        {/* fourth row */}
        <div className={styles.secondRow}>
          <Typography
            children="Top Selling Products"
            variant="h3"
            color="black"
            style={{ marginBottom: "0.8125em", maxWidth: "195px" }}
          />

          <BooksCarousel />
        </div>
      </div>

      <NewsLetter />
    </main>
  );
}
