import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import Link from "next/link";

import Typography from "../components/Typography/Typography";
import { TextInput } from "../components/SelectInput/SelectInput";
import PublicLayout from "../components/Layout/PublicLayout";
import Button from "../components/Button/Button";

const schema = Yup.object().shape({
  email: Yup.string().required("Please type your address").trim(),
  password: Yup.string().required("Please type your city").trim(),
});

function Login() {
  return (
    <div
      style={{
        backgroundColor: "#E5E5E5",
        display: "flex",

        justifyContent: "center",
        alignContent: "center",
        padding: "4rem",
        flexDirection: "column",
        alignItems: "center",
        minHeight: "70vh",
      }}
    >
      <Typography variant="h2" style={{ marginBottom: "2em" }}>
        Login
      </Typography>
      <div
        style={{
          width: "440px",
          height: "345px",
          backgroundColor: "white",
          justifyContent: "center",
          alignContent: "center",
          display: "flex",
          flexDirection: "column",
          padding: "1em",
        }}
      >
        <Formik
          initialValues={{
            email: "",
            password: "",
          }}
          validationSchema={schema}
          enableReinitialize={true}
          onSubmit={(values) => {
            console.log(values);
          }}
        >
          {(props) => {
            const {
              handleChange,
              values,
              handleSubmit,
              errors,
              touched,
            } = props;

            return (
              <div
                style={{
                  backgroundColor: "#fff",
                  display: "flex",
                  flexDirection: "column",
                  justifyItems: "center",
                  padding: "1em",
                }}
              >
                <TextInput
                  placeholder="Email"
                  onChange={handleChange("email")}
                  value={values.email}
                  style={{ width: "100%" }}
                />
                <TextInput
                  placeholder="Password"
                  onChange={handleChange("password")}
                  value={values.password}
                  style={{ width: "100%" }}
                />

                <Typography
                  variant="body3"
                  style={{ marginBottom: ".2em", textAlign: "right" }}
                  color="blue"
                >
                  Forgot password?
                </Typography>

                <Link href="/">
                  <Button
                    text="Send"
                    style={{
                      padding: "1em 2em",
                      marginTop: "2em",

                      width: "100%",
                      height: "48px",

                      background: "#0c8dba",
                      border: "none",
                      borderRadius: "4px",
                      color: "#fff",
                    }}
                  />
                </Link>
              </div>
            );
          }}
        </Formik>
      </div>
    </div>
  );
}

Login.getLayout = (page) => <PublicLayout>{page}</PublicLayout>;

export default Login;
