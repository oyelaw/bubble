import React from "react";
import Image from "next/image";
import { Formik } from "formik";
import * as Yup from "yup";

import Typography from "../components/Typography/Typography";
import SelectInput, {
  TextInput,
  TextArea,
} from "../components/SelectInput/SelectInput";
import AccountSidebar from "../components/Partials/AccountSidebar";
import Button from "../components/Button/Button";

const schema = Yup.object().shape({
  address: Yup.string().required("Please type your address").trim(),
  city: Yup.string().required("Please type your city").trim(),
  state: Yup.string().required("Please select your state").trim(),
  fullname: Yup.string().required("Please type your fullname").trim(),
  phoneNumber: Yup.string().required("Please type your phone number").trim(),
});
const containerStyle = {
  backgroundColor: "#E5E5E5",
  display: "grid",
  gridTemplateColumns: "1fr 4fr",
  gridGap: "1em",
  padding: "1.7em",
  flex: 1,
};

const leftPane = {
  backgroundColor: "#fff",
  maxHeight: "310px",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const rightPane = {
  backgroundColor: "#fff",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const iconsText = {
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
};

const active = {
  backgroundColor: "#F1F4F4",
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
  padding: ".4em",
};

const buttonStyle = {
  width: "100%",
  backgroundColor: "#fff",
  color: "#0c8dba",
  alignSelf: "center",
  borderTop: "1px solid #E0E0E0",
  padding: "1em",
  marginTop: ".5em",
};

const boxStyle = {
  height: "192px",
  background: "rgba(12, 141, 186, 0.03)",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
};

const EditButtonStyle = {
  width: "100%",
  backgroundColor: "inherit",
  color: "#0c8dba",
  textAlign: "left",
  marginTop: ".5em",
};

const profileRow = {
  display: "flex",
  justifyItems: "space-between",
};
export default function Account() {
  const [showTable, setTable] = React.useState(false);
  return (
    <div style={containerStyle}>
      <AccountSidebar />
      <div>
        <div style={{ backgroundColor: "#E5E5E5", width: "100%" }}>
          <Typography
            children="Go Back"
            variant="body4"
            color="blue"
            style={{ marginBottom: "1em" }}
          />
        </div>
        <div style={rightPane}>
          <Typography
            children="Address Book"
            variant="h3"
            color="black"
            style={{ marginBottom: "1em" }}
          />
          <div
            style={{
              display: "flex",
              borderTop: "1px solid #E0E0E0",
              paddingTop: "1em",
              flexDirection: "column",
              minHeight: "500px",
            }}
          >
            <div>
              <div
                tyle={{
                  backgroundColor: "#fff",
                  display: "flex",
                  flexDirection: "columen",
                  justifyItems: "center",
                  padding: "1em",
                }}
              >
                <div>
                  <Formik
                    initialValues={{
                      address: "",
                      city: "",
                      state: "",
                      fullname: "",
                      phoneNumber: "",
                    }}
                    validationSchema={schema}
                    enableReinitialize={true}
                    onSubmit={(values) => {
                      console.log(values);
                    }}
                  >
                    {(props) => {
                      const {
                        handleChange,
                        values,
                        handleSubmit,
                        errors,
                        touched,
                      } = props;

                      return (
                        <div
                          style={{
                            backgroundColor: "#fff",
                            display: "flex",
                            flexDirection: "column",
                            justifyItems: "center",
                            padding: "1em",
                          }}
                        >
                          <div style={{ display: "flex" }}>
                            <TextInput
                              placeholder="Fullname"
                              onChangeText={handleChange("fullname")}
                              value={values.fullname}
                              style={{ width: "50%" }}
                            />
                            <TextInput
                              placeholder="Phone number"
                              onChangeText={handleChange("phoneNumber")}
                              value={values.phoneNumber}
                              style={{ marginLeft: ".5em", width: "50%" }}
                            />
                          </div>

                          <TextArea
                            placeholder="Address"
                            onValueChange={handleChange("address")}
                            value={values.address}
                            style={{ width: "100%" }}
                            row={5}
                          />
                          <div style={{ display: "flex" }}>
                            <TextInput
                              placeholder="City"
                              onChangeText={handleChange("city")}
                              value={values.city}
                              style={{ width: "50%" }}
                            />
                            <TextInput
                              placeholder="State"
                              onChangeText={handleChange("state")}
                              value={values.state}
                              style={{ marginLeft: ".5em", width: "50%" }}
                            />
                          </div>
                          <Button
                            text="Add New Address"
                            style={{
                              padding: "1em 2em",
                              marginTop: "2em",

                              width: "100%",
                              height: "48px",

                              background: "#0c8dba",
                              border: "none",
                              borderRadius: "4px",
                              color: "#fff",
                            }}
                          />
                        </div>
                      );
                    }}
                  </Formik>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
