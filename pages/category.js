import React from "react";
import { LayoutWithSearchBarInHeader } from "../components/Layout/PublicLayout";
import SideBar from "../components/Partials/Sidebar";
import ProductDetail from "../components/ProductDetail/ProductDetail";
import Typography from "../components/Typography/Typography";
import { Pagination } from "../components/Button/Button";
import styles from "../styles/Category.module.css";

const ProductData = [
  {
    image: "/assets/product4.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
  {
    image: "/assets/product1.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
  {
    image: "/assets/product5.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
  {
    image: "/assets/product3.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
  {
    image: "/assets/product1.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
  {
    image: "/assets/product4.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
  {
    image: "/assets/product2.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
  {
    image: "/assets/product3.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
  {
    image: "/assets/product1.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
  {
    image: "/assets/product3.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
  {
    image: "/assets/product4.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
  {
    image: "/assets/product1.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
  {
    image: "/assets/product5.png",
    description: "Baby Trend Serene Nursery Center, Hello Kitty Classic Dot",
    price: "N13,000.00",
    rating: 3,
  },
];

const mainStyle = {
  display: "flex",
};

function Product() {
  return (
    <div className={styles.container}>
      <ul className={styles.crumb}>
        <li>Home</li>
        <li> > </li>
        <li>Clothing</li>
        <li> > </li>
        <li>Boy Clothings</li>
      </ul>

      <div style={mainStyle}>
        <div style={{ flex: "1" }}>
          <SideBar />
        </div>

        <div style={{ flex: "4", marginLeft: "1em" }}>
          <Typography
            variant="h3"
            color="black"
            style={{ marginBottom: "1.18em" }}
          >
            Boys Clothing
          </Typography>
          <Typography
            variant="body3"
            color="grey"
            style={{ marginBottom: "1.18em" }}
          >
            Showing 133 results for Boy Clothings
          </Typography>

          <div className={styles.productPane}>
            {ProductData.map((product, index) => {
              return (
                <ProductDetail
                  image={product.image}
                  price={product.price}
                  description={product.description}
                  showAddcart
                  showPrice
                />
              );
            })}
          </div>
        </div>
      </div>
      <div style={{ justifyContent: "center", display: "flex" }}>
        <Pagination />
      </div>
    </div>
  );
}

Product.getLayout = (page) => (
  <LayoutWithSearchBarInHeader>{page}</LayoutWithSearchBarInHeader>
);

export default Product;
