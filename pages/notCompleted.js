import React from "react";
import Link from "next/link";

import Typography from "../components/Typography/Typography";
import Button from "../components/Button/Button";
import { LayoutWithSearchBarInHeader } from "../components/Layout/PublicLayout";

function Success() {
  return (
    <div
      style={{
        backgroundColor: "#E5E5E5",
        display: "flex",

        justifyContent: "center",
        alignContent: "center",
        padding: "4rem",
      }}
    >
      <div
        style={{
          width: "728px",
          height: "502px",
          backgroundColor: "white",
          justifyContent: "center",
          alignContent: "center",
          display: "flex",
          flexDirection: "column",
          padding: "2em",
        }}
      >
        <div
          style={{
            justifyContent: "center",
            alignContent: "center",
            display: "flex",
            marginBottom: "1.3125em",
          }}
        >
          <div
            style={{
              backgroundColor: "#D00000",
              height: "76px",
              weight: "76px",
              padding: "2em",
              borderRadius: "50%",
              alignItems: "center",
              display: "flex",
            }}
          >
            <img src="/assets/x.png" height="28px" width="38px" />
          </div>
        </div>
        <Typography
          variant="h2"
          children="Sorry your purchase wasn’t completed"
          color="#0c8dba"
          style={{ marginBottom: "1.375em" }}
        />
        <Typography
          variant="body2"
          children="Check you payment method and try again."
          color="black"
          style={{ marginBottom: "2em" }}
        />
        <Link href="/">
          <Button
            text="Go Back"
            style={{ height: "48px", width: "100%", margin: "2em 0" }}
          />
        </Link>
      </div>
    </div>
  );
}

Success.getLayout = (page) => (
  <LayoutWithSearchBarInHeader>{page}</LayoutWithSearchBarInHeader>
);

export default Success;
