import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";

import Typography from "../components/Typography/Typography";
import Button from "../components/Button/Button";
import AccountSidebar from "../components/Partials/AccountSidebar";
import SelectInput, { TextInput } from "../components/SelectInput/SelectInput";

const schema = Yup.object().shape({
  address: Yup.string().required("Please type your address").trim(),
  city: Yup.string().required("Please type your city").trim(),
  state: Yup.string().required("Please select your state").trim(),
  fullname: Yup.string().required("Please type your fullname").trim(),
  phoneNumber: Yup.string().required("Please type your phone number").trim(),
});

const containerStyle = {
  backgroundColor: "#E5E5E5",
  display: "grid",
  gridTemplateColumns: "1fr 4fr",
  gridGap: "1em",
  padding: "1.7em",
};

const leftPane = {
  backgroundColor: "#fff",
  maxHeight: "310px",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const rightPane = {
  backgroundColor: "#fff",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const active = {
  backgroundColor: "#F1F4F4",
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
  padding: ".4em",
};

const buttonStyle = {
  width: "100%",
  backgroundColor: "#fff",
  color: "#0c8dba",
  alignSelf: "center",
  borderTop: "1px solid #E0E0E0",
  padding: "1em",
  marginTop: ".5em",
};

const boxStyle = {
  height: "192px",
  background: "rgba(12, 141, 186, 0.03)",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
};

const EditButtonStyle = {
  width: "100%",
  backgroundColor: "inherit",
  color: "#0c8dba",
  textAlign: "left",
  marginTop: ".5em",
};
export default function Account() {
  return (
    <div style={containerStyle}>
      <AccountSidebar />
      <div>
        <div style={{ backgroundColor: "#E5E5E5", width: "100%" }}>
          <Typography
            children="Go Back"
            variant="body4"
            color="blue"
            style={{ marginBottom: "1em" }}
          />
        </div>
        <div style={rightPane}>
          <Typography
            children="Add Child"
            variant="h3"
            color="black"
            style={{ marginBottom: "1em" }}
          />
          <div
            style={{
              display: "flex",
              borderTop: "1px solid #E0E0E0",
              paddingTop: "1em",
            }}
          >
            <Formik
              initialValues={{
                address: "",
                city: "",
                state: "",
                fullname: "",
                phoneNumber: "",
              }}
              validationSchema={schema}
              enableReinitialize={true}
              onSubmit={(values) => {
                console.log(values);
              }}
            >
              {(props) => {
                const {
                  handleChange,
                  values,
                  handleSubmit,
                  errors,
                  touched,
                } = props;

                return (
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      width: "100%",
                    }}
                  >
                    <TextInput
                      placeholder="Fullname*"
                      onChangeText={handleChange("fullname")}
                      value={values.fullname}
                      style={{ width: "100%" }}
                    />

                    <div
                      style={{
                        display: "flex",
                        marginTop: "2em",
                        alignItems: "center",
                      }}
                    >
                      <SelectInput
                        placeholder="Gender"
                        onChangeText={handleChange("gender")}
                        value={values.city}
                        options={[]}
                        style={{
                          width: "50%",
                          backgroundColor: " rgba(12, 141, 186, 0.02)",
                          border: "1px solid #E0E0E0",
                          height: "48px",
                        }}
                      />
                      <TextInput
                        placeholder="Phone number*"
                        onChangeText={handleChange("state")}
                        value={values.state}
                        style={{ marginLeft: ".5em" }}
                      />
                    </div>
                    <Button
                      text="Add Child"
                      style={{
                        padding: "1em 2em",
                        margin: "2em 0",

                        width: "100%",

                        background: "#0c8dba",
                        border: "none",
                        borderRadius: "4px",
                        color: "#fff",
                      }}
                    />
                  </div>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
}
